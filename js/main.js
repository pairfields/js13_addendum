"use strict";
class TopSellerSlide {
  constructor(picture, pictureName, sellerAvatar, sellerName, instantPrice) {
    this.picture = picture;
    this.pictureName = pictureName;
    this.sellerAvatar = sellerAvatar;
    this.sellerName = sellerName;
    this.instantPrice = instantPrice;
    this.currentBid = this.getRandomArbitrary(0, 5).toFixed(2);
    this.transfer = 1274.22;
    this.changeToUSD();
    this.endtime = this.getRandomArbitrary(60000, 86400000) + Date.parse(new Date());
  }
  changeToUSD() {
    this.currentBidUSD = (this.currentBid * this.transfer).toFixed(2);
  }

  // нужна для эмуляции получения различных данных с сервера
  getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }
  render() {
    const element = document.createElement("div");
    element.innerHTML = `<div class="seller__slider__item">
		<div
			class="seller__slider__item__inner">
			<div
				class="seller__slider__picture">
				<img src=${this.picture}
					alt="">
			</div>
			<div class="seller__slider__active">
				<div class="seller__slider__info">
					<div class="title__super__big">
						${this.pictureName}®
					</div>
					<div class="seller__info">
						<div
							class="seller__info__block">
							<div class="info__seller__avatar">
								<img src=${this.sellerAvatar} alt="">
							</div>
							<div class="info__text">
								<h4 class="substr substr__mini">Creator
								</h4>
								<h4 class="title__mini">
									${this.sellerName}</h4>
							</div>
						</div>

						<div
							class="seller__info__block">
							<div class="info__picture">
								<img src="resources/user.png" alt="">
							</div>
							<div class="info__text">
								<h4 class="substr substr__mini">Instant
									price</h4>
								<h4 class="title__mini">${this.instantPrice} ETH</h4>
							</div>
						</div>
					</div>
				</div>
				<div
					class="seller__slider__price">
					<h4 class="current__bid">Current Bid</h4>
					<h3 class="price__bid">${this.currentBid} ETH</h3>
					<h3 class="substr price__substr">$${this.currentBidUSD}
					</h3>
					<div class="promotion__timer">
						<div class="auction">Auction ending in</div>
						<div class="timer timer-${this.pictureName.replace(" ", "")}">
							<div class="timer__block">
								<span id="hours" class="number"></span>
								Hrs
							</div>
							<div class="timer__block">
								<span
									id="minutes" class="number"></span>
								mins
							</div>
							<div class="timer__block">
								<span
									id="seconds" class="number"></span>
								secs
							</div>

						</div>
					</div>
				</div>
				<button class="btn slider__btn">Place
					a bid</button>
				<button class="btn slider__btn">View
					item</button>
			</div>
		</div>
</div>`;
    document.querySelector(".seller__slider").append(element);
    //запускаем уникальный таймер для каждого элемента слайдера
    setClock(`.timer-${this.pictureName}`, this.endtime);
  }
}

new TopSellerSlide("resources/pictures/1.jpg", "Marco carrillo", "resources/sellers/01.png", "Enrico Cole", "3.5", "6.7").render();
new TopSellerSlide("resources/pictures/2.jpg", "Marco", "resources/sellers/01.png", "Enrico Cole", "1.3", "5.00").render();

new TopSellerSlide("resources/pictures/3.jpg", "Mamarco", "resources/sellers/01.png", "Enrico Cole", "5.5").render();

new TopSellerSlide("resources/pictures/4.jpg", "Carerrolo", "resources/sellers/01.png", "Enrico Cole", "2.8").render();

//таймер
function getTimeRemaining(endtime) {
  const temp = endtime - new Date(),
    hours = Math.floor((temp / (1000 * 60 * 60)) % 24),
    minutes = Math.floor((temp / (1000 * 60)) % 60),
    seconds = Math.floor((temp / 1000) % 60);

  return {
    total: temp,
    hours: hours,
    minutes: minutes,
    seconds: seconds,
  };
}

function getZero(num){
	if(num >= 0 && num <10){
		return `0${num}`
	}else{
		return num
	}
}

function setClock(selector, endtime) {
  selector = selector.replace(" ", "");
  console.log(selector);
  const timer = document.querySelector(selector),
    hours = timer.querySelector("#hours"),
    minutes = timer.querySelector("#minutes"),
    seconds = timer.querySelector("#seconds"),
    timeInterval = setInterval(updateClock, 1000);

  updateClock();

  function updateClock() {
    const temp = getTimeRemaining(endtime);

    hours.innerHTML = getZero(temp.hours);
    minutes.innerHTML = getZero(temp.minutes);
    seconds.innerHTML = getZero(temp.seconds);

		if(temp.total<=0){
			clearInterval(timeInterval);
		}
  }
}
// добавляем слушатель события сразу на все кнопки на странице путем делегирования с body,
// при необходимости, можно добавить разные слушатели на разные кнопки
// закрываться будет при нажатии на кнопку выхода или на кнопки действия внутри модального окна, а также на любое свободное пространство вне модального окна
const modal = document.querySelector(".modal");
document.body.addEventListener("click", (e) => {
  if (
    e.target &&
    (e.target.classList.contains("btn") || e.target.classList.contains("modal__exit__btn") || e.target.classList.contains("modal__exit__btn__img") || e.target.classList.contains("modal__inner"))
  ) {
    console.log(e.target);
    document.body.style.overflow = modal.classList.contains("hide") ? "hidden" : "";
    modal.classList.toggle("hide");
  }
});
$(function () {
  $(".seller__slider").slick({
    infiniti: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    nextArrow: '<button class="slick-next-top slick-btn"><img src="resources/vector (Stroke) right.png" alt=""></button>',
    prevArrow: '<button class="slick-prev-top slick-btn"><img src="resources/vector (Stroke) left.png" alt=""></button>',
  });
});

class SellersCard {
  constructor(topNumber, sellerAvatar, sellerName, sellerWallet) {
    this.topNumber = topNumber;
    this.sellerAvatar = sellerAvatar;
    this.sellerName = sellerName;
    this.sellerWallet = sellerWallet;
  }
  render() {
    const element = document.createElement("div");
    element.innerHTML = `
		<div class="sellers__content__block">
					<div class="sellers__block__top">
						<img src=${this.topNumber} alt="">
						<a href="#"><img src="resources/icons.svg" alt=""></a>
					</div>
					<div class="sellers__block__middle">
						<a href="#"><div class="user__avatar">
							<img class="photo" src=${this.sellerAvatar} alt="">
							<img class="user__icon" src="resources/diamond.svg" alt="">
						</div></a>
					</div>
					<div class="sellers__block__buttom">
						<h4 class="title__mini">${this.sellerName}</h4>
						<h4 class="substr__mini">${this.sellerWallet} ETH</h4>
					</div>
				</div>
		`;
    document.querySelector(".sellers__content").append(element);
  }
}

//крайне прикольным способом рендерим карточки, не уверен, что кто-то реально так делает, но выглядит достаточно компактно и понятно
let names = [
  { "Edd Harris": "2.456" },
  { "Odell Hane": "2.367" },
  { "Marlee Kuphal": "2.108" },
  { "Payton Kunde": "1.764" },
  { "Payton Buckridge": "1.529" },
  { "Laura Scott": "1.267" },
  { "Julie Mills": "1.183" },
  { "Patricia Armstrong": "0.963" },
];
for (let i = 0; i < 8; i++) {
  new SellersCard(`resources/top/0${i + 1}.svg`, `resources/users/0${i + 1}.png`, ...Object.keys(names[i]), ...Object.values(names[i])).render();
}

$(function () {
  $(".sellers__content").slick({
    infiniti: true,
    slidesToShow: 5,
    slidesToScroll: 2,
    arrows: true,
    dots: false,
    nextArrow: '<button class="slick-next-bottom slick-btn"><img src="resources/vector (Stroke) right.png" alt=""></button>',
    prevArrow: '<button class="slick-prev-bottom slick-btn"><img src="resources/vector (Stroke) left.png" alt=""></button>',
  });
});
